// Create an express app
var express = require('express'); // express was installed by npm
var app = express(); // Create a new express application instance. This stores
					 // all information about our app.

// set up handlebars view engine
var handlebars = require('express-handlebars') // handlebars was installed by npm
	.create({ defaultLayout:'main' }); // Handlebars audomatically finds main.handlebars
									   // because of the app directory structure

app.engine('handlebars', handlebars.engine); // Tell the app to use handlebars to
											 // compile .handlebars files
app.set('view engine', 'handlebars'); // Tell the app to use handlebars by default
									  // to compile views.

// Tell the app to look for an environment variable $PORT and use the value
// as the port number. If this fails for any reason, run on port 3000.
app.set('port', process.env.PORT || 3000);

// Tell the app that our static assets (images, sylesheets, javascript, etc.)
// can be found in the public folder at our project root (e.g. ./public)
app.use(express.static(__dirname + '/public'));

var fortuneCookies = [
	"Conquer your fears or they will conquer you.",
	"Rivers need springs.",
	"Do not fear what you don't know.",
	"You will have a pleasant surprise.",
	"Whenever possible, keep it simple.",
];

// HTTP method handlers receive a request object (req) and a response object (res).
// We can use req to handle rendering views in different circumstances
// (for example, mobile vs desktop).
// We set all of the information that the app needs to render the correct view
// inside of res--the contents of res determine what the user sees.
app.get('/', function(req, res){
	console.log("request headers:",req.headers);
    res.set('Content-Type','text/html'); // Set the HTTP response header Content-Type field
	res.render('home',{customer:"<em>Buttercup</em>"}); // Handlebars will replace {{{customer}}} in
														// home.handlebars with "<em>Buttercup</em>"
});

app.get('/about', function(req,res){
	var randomFortune =
		fortuneCookies[Math.floor(Math.random() * fortuneCookies.length)];
	res.render('about', { fortune: randomFortune});
});

// Express allows us to set default handlers for HTTP response codes using middleware.
// The handler will be triggered any time the code is encountered (e.g. the handler
// for 404 will run every time a user tries to access content that Express
// cannot find).

// 404 catch-all handler (middleware)
// req and res are the same objects as in our get() handlers. next is a pointer
// to the next middleware function in the app, in this case the 500 error
// handler. Calling next() here will immediately invoke the 500 error handler.
app.use(function(req, res, next){
	res.status(404);	// Set our response header to status 404
	res.render('404');
});

// 500 error handler (middleware)
app.use(function(err, req, res, next){
	console.error(err.stack);	// err is a server error, so log to see what went wrong
	res.status(500);			// Set our response header to status 500
	res.render('500');
});

// Begin listening for user requests on the port we set previously. The server
// will run until it crashes or it is interrupted with Ctrl-C.
app.listen(app.get('port'), function(){
  console.log( 'Express started on http://localhost:' +
    app.get('port') + '; press Ctrl-C to terminate.' );
});
