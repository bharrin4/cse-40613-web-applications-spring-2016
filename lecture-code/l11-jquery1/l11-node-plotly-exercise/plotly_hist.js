"use strict";

// Generate 2n random normal numbers
var normal = function(n) {
    var i, u, v, s, t, x, root, theta;

    x = [];

    for (i = 0; i < n; i++) {
        u = 0;

        while (u === 0) {
            u = Math.random();
        }

        v = Math.random();

        root = Math.sqrt(-2 * Math.log(u));
        theta = 2 * Math.PI * v;

        s = root * Math.cos(theta);
        t = root * Math.sin(theta);

        x.push(s);
        x.push(t);
    }

    return x;
};

var APP = (function() {
    // Set your username and API key here
    var plotly = require('plotly')('yourUsername', 'yourKey');

    // Set the initial x-values with 100 seed values
    // Set the type to histogram
    // Set your Streaming API Token here
    var data = [{x:[], type:"histogram", stream:{token:'yourToken', maxpoints:500}}];

    var graphOptions = {fileopt : "overwrite", filename : "example"};

    plotly.plot(data,graphOptions,function() {
        // Set your Streaming API Token here
        var stream = plotly.stream('yourToken', function (err,res) {
            console.log(err,res);
            clearInterval(loop);
        });
        var i=0;
        var loop = setInterval(function () {
            //Calculate the next round of random normal numbers and write them to plotly
            var streamObject = JSON.stringify();
            stream.write(streamObject+'\n');
            i++;
        }, 2000);
    });
}());