/**
 * Created by jesus on 3/22/16.
 */
"use strict";

$(function () {
    var fbname = "blistering-fire-6598";
    var ref = new Firebase("https://" + fbname + ".firebaseio.com/restaurants");
    var sorinsMenuRef = ref.child('sorins').child('menu');

// use value event
    sorinsMenuRef.once('value', function (snapshot) {
        console.log(snapshot.exists());
        console.log(snapshot.key());
        console.log(snapshot.val());
        var menuArray = snapshot.val();
        console.log(menuArray[0]);
    });

// use value event to keep list of restaurants sorted by name
    ref.orderByChild('name').on('value', function (snapshot) {
        var $restList = $('#rest-list');
        $restList.html("");
        snapshot.forEach(function (childSnapshot) {
            console.log(childSnapshot.key(), childSnapshot.val());
            // Two ways of appending a li:
            //$restList.append($('<li class="list-group-item">' + childSnapshot.val()["name"] + '</li>'));
            $('<li class="list-group-item">' + childSnapshot.val()["name"] + '</li>').appendTo($restList);
        });
    });


// write a new restaurant
    ref.update({
        "rohrs": {
            city: "South Bend",
            lat: "41.756634",
            lng: "-86.238832",
            menu: "Coming soon",
            name: "Rohr's",
            site: "http://morrisinn.nd.edu/dining/rohrs/",
            phone: "(574) 631-2000",
            state: "Indiana",
            street: "1 Notre Dame Ave.",
            zip: "46556"
        }
    }, function (err) {
        if (err) {
            console.log("Could not update Rohr's restaurant data!");
        } else {
            console.log("Successfully added Rohr's to restaurant list");
        }
    });

// push to create a unique order-id
    var ordersRef = new Firebase("https://" + fbname + ".firebaseio.com").child('orders');
    var myOrderRef = ordersRef.push({
        restaurantId: "sorins",
        totalItems: 0,
        totalPrice: 0,
        submitted: false,
        items: false
    }, function (err) {
        if (err) {
            console.log("Could not add order for Sorin's!");
        } else {
            console.log("Successfully added order for Sorin's");
        }
    });

//myOrderRef.child('items').update({
//    "Honey Glazed Pork Belly": {
//        qty : "1",
//        price : "8.99"
//    }
//});

    var porkBellyQty = 2, porkBellyPrice = 8.99;
    myOrderRef.child('items').update({
        "Honey Glazed Pork Belly": {
            price: porkBellyPrice,
            qty: porkBellyQty
        }
    }, function (err) {
        if (err) {
            console.log("Failed to update items with Pork Belly");
        } else {
            myOrderRef.child('totalItems').transaction(function (currentValue) {
                return (currentValue || 0) + porkBellyQty;
            });
            myOrderRef.child('totalPrice').transaction(function (currentValue) {
                return (currentValue || 0) + porkBellyPrice * porkBellyQty;
            });
        }
    });

// Let's add Aged Filet Mignon to our order

    var filetMignonQty = 1, filetMignonPrice = 20.99;
    myOrderRef.child('items').update({
        "Aged Filet Mignon": {
            price: filetMignonPrice,
            qty: filetMignonQty
        }
    }, function (err) {
        if (err) {
            console.log("Failed to update items with Filet Mignon");
        } else {
            myOrderRef.child('totalItems').transaction(function (currentValue) {
                console.log("current total items", currentValue);
                return (currentValue || 0) + filetMignonQty;
            });
            myOrderRef.child('totalPrice').transaction(function (currentValue) {
                return (currentValue || 0) + filetMignonPrice * filetMignonQty;
            });
        }
    });

});