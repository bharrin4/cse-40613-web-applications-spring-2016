/**
 * Created by jesus on 4/12/16.
 */
"use strict";

$(function() {
    var url = "http://ec2-54-89-76-246.compute-1.amazonaws.com/api/hotspots/?limit=10";
    var venues = $(".links-list"), item;
   $.getJSON(url)
    .done(function(data) {
        console.log("hotspots returned:",data);
        data.forEach(function(venue){
            console.log("venue",venue);
            item = '<li class="list-group-item link">'
                  +venue.name
                  +'<span class="badge">'
                  +venue.votes
                  +'</span>';
            venues.append($(item));
        })
    })
    .fail(function(err){
        console.log("hotspots failed:",err);
    });
});