/**
 * Created by jesus on 2/4/16.
 */
"use strict";

var polygon, square, attr;
polygon = {
    vertices: [
        {x: 0, y: 0},
        {x: 1, y: 0},
        {x: 1, y: 1},
        {x: 0, y: 1}],
    order: function () {
        return this.vertices.length;
    },
    perimeter: function () {
        // how do you compute the perimeter of a polygon?
        var result = 0, order = this.order(), i, a, b, xd, yd;
        for (i = 0; i < this.order(); i++) {
            a = this.vertices[i % order];
            b = this.vertices[(i + 1) % order];
            xd = a.x - b.x;
            yd = a.y - b.y;
            result += Math.sqrt(xd * xd + yd * yd);
        }
        return result;
    }
};

console.log(polygon.order(),polygon.perimeter());

square = Object.create(polygon);
square.perimeter = function() {
    var a, b, xd, yd;
    a = this.vertices[0];
    b = this.vertices[1];
    xd = a.x - b.x;
    yd = a.y - b.y;
    return 4*(xd*xd+yd*yd);
};

console.log(square.order(),square.perimeter());

for (attr in square) {
    if (square.hasOwnProperty(attr)) {
        console.log(attr, "belongs to square on its own right");
    }
    else {
        console.log(attr, "is inherited by square");
    }
}
/*
APP.entry=(function() {
    // define common vars
    // define common functions
    return {
        method1 : function(arg) {},
        method2 : function() {}
    }
}());*/