/**
 * Created by jesus on 2/2/16.
 */
"use strict";
// illustrate use of Arrays

var arr, i;
arr=['oats','peas','beans'];
arr.push('bananas');


for (i=0; i<arr.length; i++) {
    console.log(arr[i]);
}
console.log(typeof arr,Array.isArray(arr));
arr.splice(2,1); //remove 1 element starting at arr[2]
console.log(arr.join(" "));
console.log(arr.sort());
