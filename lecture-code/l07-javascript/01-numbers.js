/**
 * Created by jesus on 2/1/16.
 */
"use strict";
/* Example showing arithmetic in js */

var n1=0.195, n2=0.023;
var myCeil={'totalWithCeil':Math.ceil((n1*100+n2*100))};
var myFloor={'totalWithFloor':Math.floor((n1*100+n2*100))};
var myFP={'totalWithFP':n1+n2};

console.log(myFP);
console.log(myCeil);
console.log(myFloor);

console.log(Math.pow(2,53)-1);
console.log(Math.random(),Math.random());
console.log(Math.sin(Math.E), Math.cos(2*Math.PI));

var r=Math.random()*2*Math.PI;
console.log("Sin of random number %d is %d",r, Math.sin(r));

var name="Jesus";
console.log("Hello %s",name);

console.log("JSON = %j",myFloor);