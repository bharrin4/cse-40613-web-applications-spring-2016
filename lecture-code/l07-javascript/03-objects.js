/**
 * Created by jesus on 2/2/16.
 */
"use strict";

// Illustrates objects

var frodo={
    name : "Frodo Baggins",
    'goto' : "Mordor",
    mealsPerDay : 5,
    species : "Hobbit"
};

var sam={
    name : "Samwise Gamgee",
    'goto' : "The Shire",
    mealsPerDay : 6,
    species : "Hobbit",
    boss: frodo
};
console.log(sam);
console.log(frodo);

var fortune = function () {
    var result = "";
    switch(this.goto) {
        case "The Shire":
            result = "Lucky you!";
            break;
        case "Mordor":
            result = "May the Force be with you!";
            break;
        default:
            result = "Unknown destination!";
    }
    return result;
};

sam.fortune = fortune;
frodo.fortune = fortune;
console.log(sam['goto'],":",sam.fortune());
console.log(frodo['goto'],":",frodo.fortune());