"use strict";

var graphNode = function(id) {

    return {
        id: id,
        visited: false
    };
};

var graph = function() {

    var that, nodes, edges;

    that = {};

    nodes = [];

    edges = {};

    that.addNode = function(n) {
        nodes.push(n);
    };

    that.getNode = function(i) {
        return nodes[i];
    };

    that.addEdges = function(n, e) {
        edges[n.id] = e;
    };

    that.getEdges = function(n) {
        return edges[n.id];
    };

    that.unsetVisited = function() {
        nodes.map(function(n) {n.visited = false;});
    };

    that.dfs = function(n) {
        var self, recurse, result;

        recurse = function(v, g) {
            var i, w, e, reachable;

            reachable = [v.id];

            v.visited = true;

            e = g.getEdges(v);

            for (i = 0; i < e.length; i++) {
                w = e[i];

                if (!w.visited) {
                    reachable = reachable.concat(recurse(w, g));
                }
            }

            return reachable;
        };

        self = this;

        result = recurse(n, self);

        this.unsetVisited();

        return result;
    };

    return that;
};


var g = graph();

var n0, n1, n2, n3, n4, n5, n6;

n0 = graphNode(0);
n1 = graphNode(1);
n2 = graphNode(2);
n3 = graphNode(3);
n4 = graphNode(4);
n5 = graphNode(5);
n6 = graphNode(6);

g.addNode(n0);
g.addNode(n1);
g.addNode(n2);
g.addNode(n3);
g.addNode(n4);
g.addNode(n5);
g.addNode(n6);

g.addEdges(n0, [n2, n5]);
g.addEdges(n1, [n1, n5]);
g.addEdges(n2, [n0, n3]);
g.addEdges(n3, [n0, n1]);
g.addEdges(n4, [n6]);
g.addEdges(n5, [n1]);
g.addEdges(n6, [n4]);


console.log(g.dfs(g.getNode(0)));

console.log(g.dfs(g.getNode(4)));

