/**
 * Created by jesus on 4/10/16.
 */
"use strict";
var firebaseSecret = require('./config').firebaseSecret;
var foursquareClientID = require('./config').foursquareClientID;
var foursquareSecret = require('./config').foursquareSecret;
var rest = require('restler');
var express = require('express');
var fsBase = "https://api.foursquare.com/v2/venues/explore/?";

var defaultLat = "41.6888997",
    defaultLng = "-86.2260031"; // South Bend

var app = express();

app.set('port', process.env.PORT || 3000); // 8000 for SSL
app.use(express.static(__dirname + '/public'));

// allow api to be called from other sites
app.use('/api', require('cors')());

// auxiliary functions to handle Foursquare requests

function getFSqueryBody() {
    return "client_id=" + foursquareClientID
        + "&client_secret=" + foursquareSecret
        + "&v=20140806"
        + "&m=foursquare"
        + "&section=drinks"
        + "&sortByDistance=1";
}

function processData(data) {
    var responseData = [];
    console.log(data.response.groups.length);
    var group,
        item,
        items,
        itemData,
        groups = data.response.groups,
        numGroups = groups.length;
    for (group of groups) {
        //console.log(group.items);
        for (item of group.items) {
            var location = item.venue.location;
            itemData = {
                name: item.venue.name,
                location: {
                    address: location.address,
                    lat: location.lat,
                    lng: location.lng,
                    city: location.city,
                    state: location.state
                },
                category: item.venue.categories[0].shortName || "unknown",
                votes: item.venue.stats.checkinsCount,
                id: item.venue.id
            };
            //console.log(itemData);
            responseData.push(itemData);
        }
    }
    responseData.sort(function(a,b){
        return +b.votes - +a.votes;
    });
    return responseData;
}

app.get('/api/hotspots', function(req, response) {
    var fsquery = getFSqueryBody();
    var rquery = req.query;
    var rlimit = rquery.limit;
    console.log("You are asking for hotspots near lat,lng:",rquery.lat,rquery.lng);
    console.log("Or near",rquery.near);
    console.log("Limit to this many results",rlimit);
    var fsll = rquery.lat || defaultLat;
    var lng = rquery.lng || defaultLng;
    fsll += "," + lng;
    var fsnear = rquery.near;
    console.log(fsll);
    if (fsnear) {
        fsquery += "&near="+fsnear;
    } else {
        fsquery += "&ll="+fsll;
    }
    console.log("calling Foursquare:",fsquery);
    rest.get(fsBase + fsquery)
        .on('success', function (data, res) {
            var result = processData(data);
            if (rlimit) {
                result = result.slice(0,rlimit);
            }
            response.json(result);
        })
        .on('fail', function (data, res) {
            console.log("failed:", JSON.stringify(data));
            response.json(data);
        })
        .on('error', function (err, res) {
            console.log("error:", JSON.stringify(err));
            response.json(data);
        });
});



// 404 catch-all handler (middleware)
// req and res are the same objects as in our get() handlers. next is a pointer
// to the next middleware function in the app, in this case the 500 error
// handler. Calling next() here will immediately invoke the 500 error handler.
app.use(function(req, res, next){
    res.status(404);	// Set our response header to status 404
    res.json({'error': {code:404}});
});

// 500 error handler (middleware)
app.use(function(err, req, res, next){
    console.error(err.stack);	// err is a server error, so log to see what went wrong
    res.status(500);			// Set our response header to status 500
    res.json({'error': {code:500}});
});

// Begin listening for user requests on the port we set previously. The server
// will run until it crashes or it is interrupted with Ctrl-C.
app.listen(app.get('port'), function(){
    console.log( 'Express started on http://localhost:' +
        app.get('port') + '; press Ctrl-C to terminate.' );
});

