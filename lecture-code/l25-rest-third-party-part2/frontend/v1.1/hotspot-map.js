/**
 * Created by jesus on 4/13/16.
 */
"use strict";
$(function(){
    L.mapbox.accessToken ="pk.eyJ1IjoiaXphZ3VpcnIiLCJhIjoiY2ltemloazN0MDR1YXZvbHVia215YWZyNCJ9.eP8_19TT86fJjNDY4hg7OQ";
    var map = L.mapbox.map('map', 'mapbox.emerald').setView([41.6888997,-86.2260031], 12);

    map.attributionControl
        .addAttribution('<a href="https://foursquare.com/">Places data from Foursquare</a>');

    var url = "https://ec2-54-89-76-246.compute-1.amazonaws.com/api/hotspots/?limit=15&ll=41.6888997,-86.2260031";
    var item;
    // code to be able to use Asignio authentication
    var asignioToken = "6fe0724d2800e611bceb0a9d54ca7dfd";
    AsignioAuth.initialize({
        applicationToken: asignioToken,
        containerID: "asignio"
    });
    // keep hotspots nicely organized
    var hotspots = L.layerGroup().addTo(map);
    // get hotspot data from hotspots server
    $.getJSON(url)
        .done(function (data) {
            console.log("hotspots returned:", data);
            var ctr = 0;
            data.forEach(function (venue) {
                ctr++;
                console.log("venue", venue);
                var latlng = L.latLng(venue.location.lat, venue.location.lng);
                var marker = L.marker(latlng, {
                    icon: L.mapbox.marker.icon({
                        'marker-color': '#BE9A6B',
                        'marker-symbol': 'bar',
                        'marker-size': 'large'
                    })
                })
                    .bindPopup('<strong>' + ' +<a href="https://foursquare.com/v/' + venue.id  +
                        '">' + venue.name + ' (Ranks ' + ctr + ')</strong>')
                    .addTo(hotspots);
            });
        })
        .fail(function (err) {
            console.log("hotspots failed:", err);
        });
});