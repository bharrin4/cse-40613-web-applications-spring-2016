/**
 * Created by jesus on 2/11/16.
 */
"use strict";
$(function() {
    console.log("Yay! the DOM is loaded!");
    // do cool jQuery stuff
    $('#four').hide();
    console.log("I've hidden #four");
    $('#four').fadeIn(10000);
    $('li').on('click',function(){
        $(this).remove();
    });
});
