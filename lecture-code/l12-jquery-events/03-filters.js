/**
 * Created by jesus on 2/18/16.
 */
"use strict";

$(function() {
    var $listItems = $('li');

    $listItems.filter('.hot:last').removeClass('hot');
    $('li:not(.hot)').addClass('cool');

    $listItems.each(function() {
        var $this = $(this);
        if ($this.is('.hot')) {
            $this.prepend('Priority restaurant: ');
        }
    });

    $('li:contains("Chipotle")').append(' (local E-coli)');
});