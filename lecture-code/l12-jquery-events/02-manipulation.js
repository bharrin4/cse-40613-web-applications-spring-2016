/**
 * Created by jesus on 2/16/16.
 */
"use strict";
$(function() {
    var $all = $('*');
    console.log($all);
    var $lis = $('ul');
    // text() returns content from every element in a jQ selection
    // as well as that of its descendants
    console.log("list's text:",$lis.text());
    // html() returns the html of the first selection
    console.log("list's html:",$lis.html());
    var $items = $('li');
    console.log("items' text:",$items.text());
    console.log("items' html:",$items.html());
    // create a new item
    var $newItem = $("<li id='five' class='new'>Barbici</li>");
    // add item to the end of the ul list
    $lis.append($newItem);
    // remove class from an element
    $('#three').removeClass('hot');
    // add class to an element
    $('#four').addClass('hot');
    // set CSS properties
    var backColor = $('.hot').css('background-color');
    console.log("Color was",backColor);
    $('li.hot').css({
        'background-color':"#c5a996",
        'color':"#000"
    });
    $('li.new').css({
        'background-color':"mediumvioletred",
        'color':"#111"
    });
    $lis.css({
        'list-style-type':'none'
    });
    // add some events
    // add id of a li to its text upon click or mouseover
    $items.on('mouseover click', function() {
        var ids = this.id;
        $items.children('span').remove();
        $(this).append(' <span class="priority">' + ids + '</span>');
    });

    $items.on('mouseout', function() {
       $(this).children('span').remove();
    });
});

