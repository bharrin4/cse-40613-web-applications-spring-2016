/**
 * Created by jesus on 3/1/16.
 */
"use strict";
function LiveLinks(fbname) {
    var firebase = new Firebase("https://"+ fbname + ".firebaseio.com");
    this.firebase = firebase;
    var usersRef = this.firebase.child('users');
    this.usersRef = usersRef;
    var uid;
    var linksRef = this.firebase.child('livelinks');
    this.linksRef = linksRef;
    var instance = this;

    //overridable functions
    this.onLogin = function(user) {};
    this.onLoginFailure = function() {};
    this.onLogout = function() {};
    this.onError = function(error) {};

    // long running firebase listener
    this.start = function() {
      firebase.onAuth(function (authResponse) {
          if (authResponse) {
              console.log("user is logged in");
              usersRef.child(authResponse.uid).once('value', function(snapshot) {
                  instance.user = snapshot.val();
                  instance.onLogin(instance.user);
              });
          } else {
              console.log("user is logged out");
              instance.onLogout();
          }
      });
    };

    this.uid = function() {return uid;};

    // submit links for logged in users
    this.submitLink = function(url,name) {
        if ((url.substring(0,5) !== "https") && (url.substring(0,4) !== "http")) {
            url = "http://" + url;
        }
        this.linksRef.child(btoa(url)).update({
            name: name
        }, function(error) {
            if (error) {
                instance.onError(error);
            } else {
                // many-to-many relationship between livelinks and users
                linksRef.child(btoa(url))
                        .child('users')
                        .child(instance.auth.uid)
                        .set(true);
                usersRef.child(instance.auth.uid)
                        .child('links')
                        .child(btoa(url))
                        .set(true);
            }
        });
    };
    // signup with an alias
    this.signup = function(email,password,alias) {
        this.firebase.createUser({
            email : email,
            password : password
        }, function(error, userData) {
            if (error) {
                instance.onError("Error creating user" + error);
            }
            else {
                instance.userData = userData;
                console.log("user data",userData);
                usersRef.child(userData.uid).set({
                    alias : alias
                }, function(error) {
                    if (error) {
                        instance.onError(error);
                    }
                    else {
                        instance.login(email,password);
                    }
                });
            }
        });
    };
    // login with email and password
    this.login = function(email,password) {
        this.firebase.authWithPassword({
            email: email,
            password : password
        }, function(error, authData) {
            if (!error) {
                instance.auth = authData;
                console.log("uid:", authData.uid);
            } else {
                instance.onError("login failed! " + error);
                instance.onLoginFailure();
            }
        }, {
            remember : "sessionOnly"
        });
    };
    // logout
    this.logout = function() {
        this.firebase.unauth();
        instance.auth=null;
    };
}

$(function() {
    // Use your firebase address here:
    var ll = new LiveLinks("adjective-noun-number");


    var $loginButton = $('#login-button'),
        $signupButton = $('#signup-button'),
        $logoutButton = $('#logout-button'),
        $loginForm = $('#login-form'),
        $signupForm = $('#signup-form'),
        $alerts;

    ll.onLogin = function(user) {
        console.log("in onLogin!");
        showAlert("Welcome to LiveLinks!","success");
        $loginButton.hide();
        $signupButton.hide();
        $logoutButton.show();
        $signupForm.hide();
        $loginForm.hide();
    };

    ll.onLogout = function() {
        console.log("in onLogout");
        $loginButton.show();
        $signupButton.show();
        $logoutButton.hide();
        $loginForm.hide();
        $signupForm.hide();
    };

    ll.onLoginFailure = function() {
        console.log("in onLoginFailure");
        $loginButton.show();
        $signupButton.show();
    };

    $logoutButton.on('click',function(e) {
        ll.logout();
        $logoutButton.hide();
        $loginButton.show();
        $signupButton.show();
        return false;
    });


    ll.onError = function(error) {
        showAlert(error,"danger");
    };

    $loginButton.show();
    $signupButton.show();
    $logoutButton.hide();
    $loginForm.hide();
    $signupForm.hide();

    // login forms
    $loginButton.on('click',function(e) {
        console.log("clicked login button!");
        $loginButton.hide();
        $signupButton.hide();
        $signupForm.hide();
        $loginForm.show();
        $('#login-email').val("").focus();
        $('#login-password').val("").blur();
        return false;
    });

    $loginForm.on('submit',function(e) {
        $loginForm.hide();
        e.preventDefault();
        e.stopPropagation();
        ll.login($(this).find('#login-email').val(), $(this).find('#login-password').val());
        $('#login-email').val("").blur();
        $('#login-password').val("").blur();
        return false;
    });

    $signupButton.on('click',function(e) {
        console.log("clicked signup button");
        $signupButton.hide();
        $loginButton.hide();
        $loginForm.hide();
        $signupForm.show();
        $('#signup-email').val("").focus();
        $('#signup-password').val("").blur();
        $('#signup-alias').val("").blur();
        return false;
    });

    $signupForm.on('submit', function(e) {
        $signupForm.hide();
        e.preventDefault();
        e.stopPropagation();
        ll.signup($(this).find('#signup-email').val(),
            $(this).find('#signup-password').val(),
            $(this).find('#signup-alias').val());
        $('#signup-email').val("").blur();
        $('#signup-password').val("").blur();
        $('#signup-alias').val("").blur();
    });

    $('#link-form').on('submit',function(e) {
        e.preventDefault();
        e.stopPropagation();
        console.log("auth:",ll.auth);
        if (ll.auth) {
            console.log("current user", ll.auth);
            ll.submitLink($(this).find('#link-url').val(), $(this).find('#link-name').val(), ll.auth.uid);
            $(this).find("input[type=text]").val("").blur();
        }
        else {
            ll.onError("You need to signup or login to submit links!");
        }
        return false;
    });

    ll.linksRef.on('child_added',function(snapshot) {
        var link = snapshot.val();
        var url = snapshot.key();
        console.log("child_added",url,atob(url),link);
        url = atob(url);
        $('#list').append($('<li class="hot">'+link.name+ ' : <a href="' + url + '">'+ url + '</a></li>' ));
    });

    ll.linksRef.on('child_changed',function(snapshot) {
        var link = snapshot.val();
        var url = snapshot.key();
        console.log("child_changed",url,atob(url),link);
        url = atob(url);
        var query = 'li:contains("' + url + '")';
        $(query).html(link.name + ' : <a href="' + url + '">'+ url + '</a>');
    });

    // to do: when a child is removed, need to remove link for user
    ll.linksRef.on('child_removed',function(snapshot) {
        var link = snapshot.val();
        var url = snapshot.key();
        console.log("child_removed",url,atob(url),link);
        url = atob(url);
        var query = 'li:contains("' + url + '")';
        $(query).remove();
    });

    function showAlert(message, type) {
        var $alert = (
            $('<div>')                // create a <div> element
                .text(message)          // set its text
                .addClass('alert')      // add some CSS classes and attributes
                .addClass('alert-' + type)
                .addClass('alert-dismissible')
                .hide()  // initially hide the alert so it will slide into view
        );

        /* Add the alert to the alert container. */
        $alerts = $('#alerts');
        $alerts.append($alert);

        $(alerts).on('click',function(e) {
            var $t=$(e.target);
            $t.remove();
        });

        /* Slide the alert into view with an animation. */
        $alert.slideDown();
        setTimeout(function(){$alert.hide()},3000);
    }
    // ensure no user session is active
    ll.logout();
    // start firebase auth listener only after all callbacks are in place
    ll.start();
});