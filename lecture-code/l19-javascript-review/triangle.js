"use strict";

// Check to see if the obj contains a property
// with key prop_name in its own prototype
/*var hasProp= function(obj, prop_name) {
    for (var key)
}; */

// Create a Triangle object that accepts an array of three (x,y)
// points and a name, and which has the following methods:
//      Triangle.perimeter - calculate and return the perimeter
//      Triangle.area - calculate and return the area
//      Triangle.hasPoint - find an (x,y) point in the triangle,
//                          return the point is it is in the triangle
//                          and false otherwise
//      Triangle.sortPoints - accepts and custom comparison function
//                            and sorts the points in the triangle by
//                            that function, otherwise sorts based on
//                            proximity to the origin
//      Triangle.print - log the name of the triangle and each point
//                       to the console
var Triangle = function(data) {
    //super constructor
    var that, name, v;
   /* if (hasProp(data, "name")) {
        name=data.name;
    }
    else {
        name="Generic Triangle";
    } */
    name=data.name || [];
    v=data.vertices || [];
    that={};
    that.perimeter=function() {
        return v.reduce(function(prev, current, index){
            var next=v[(index + 1) % v.length];
            return (prev + Math.sqrt(Math.pow(current.x - next.x, 2) + Math.pow(current.y - next.y, 2)));
        }, 0);
            //first argument to reduce is callback
            //second argument is previous
    };
    that.area = function() {
        var s=that.perimeter() /2;
        var reduced= v.reduce(function(prev, cur, ind) {
            var next=v[(ind + 1) % v.length];
            return prev * (s - Math.sqrt(Math.pow(cur.x - next.x, 2) + Math.pow(cur.y - next.y, 2)));
        }, s);
        return Math.sqrt(reduced);
    };
    that.hasPoint = function(p) {
        var res;
        res= v.find(function(curr){
            return (curr.x === p.x && curr.y === p.y)
        });

        if (res) {
            return res;
        }
        return false;
    };

    that.sortPoints=function(comparator) {
        var default_comparator = function(p1, p2) {
            if (p1.x > p2.x) {
                return true;
            }
            else if (p1.x === p2.x && p1.y > p2.y) {
                return true;
            }
            else {
                return false;
            }
        };
        if (!comparator) {
            comparator=default_comparator;
        }
        return v.sort(comparator);
    };
    that.print=function() {
        console.log(name);
        v.forEach(function(p, index) {
            console.log(index + ": (" + p.x + " , " + p.y + ")");
        })
    };

    return that;
};

// Create an array containing the points (0,0), (1,1), and (1,0)
var my_points = [{x:0, y:0}, {x:1, y:1}, {x:1, y:0}];

var tri=new Triangle({name: "Tri1", vertices: my_points});


// Store a function inside of this variable that accepts two
// points and returns true if the first is closer to the origin
// than the second
var my_comparator= function(p1, p2) {
    if (p1.x < p2.x) {
        return true;
    } else if (p1.x === p2.x && p1.y < p2.y) {
        return true;
    } else {
        return false;
    }
};

// Display the triangle
tri.print();

// Calculate and print the perimeter
console.log("Perimeter: ", tri.perimeter());

// Calculate and print the area

console.log("Area: ", tri.area());

// Find the point (3,7)
console.log("contains (3,7)? ", tri.hasPoint({x:3, y:7}));

// Find the point (1,0)
console.log("contains (1,0)? ", tri.hasPoint({x:1, y:0}));

// Sort using the default comparator and print


// Sort using the my_comparator function and print
console.log("Sort by my_comparator: ", tri.sortPoints(my_comparator));

// Sort using an anonymous function and print
console.log("Sort by anonymous: ", tri.sortPoints(function(p1, p2) {
    if (p1.x < p2.x) {
        return true;
    } else if (p1.x === p2.x && p1.y > p2.y) {
        return true;
    } else {
        return false;
    }
}));