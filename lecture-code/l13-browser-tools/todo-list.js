"use strict";

$(function() {
    var firebaseRef = new Firebase("https://scorching-heat-6236.firebaseio.com/todo");

    firebaseRef.on("value", function(snapshot) {
       //value events catch every change
        //success function recieves a data snapshot of the to do subtree you're looking at
        //snapshot.val will return an object with all of the children of to do
        var $ul, data, i, $li;
        var data = snapshot.val();
        console.log(data);

        $ul=$('ul');
        for (i=0; i<data.length; i++) {
            $li=('<li>'+data[i].task + " " + data[i].done + "</li>");
            $ul.append($li);
        }
    }, function(error){
        console.log(error);
    });
    // Create the to-do list here
    // 1) Read the list from Firebase
    // 2) Create an <li> for each item
    // 3) Attach each <li> to the <ul>
    
});
