"use strict";

var phonePattern = /\d{3}-\d{3}-\d{4}/;
var emailPattern = /\w+@\w+\.\w+/;

$(function() {
    // Write your phone number validation here
    // It should change the class to invalid if empty or
    // if it does not match phonePattern
    $("#phoneid").on("blur", function (event) {
        var $phone;
        console.log(event);

        $phone = $(event.target);
        console.log($phone);

        value = $phone.val();
        console.log(value);

        if (value === "" || !phonePattern().test(value)) {
            //if value is empty string or fails test for phone pattern
            console.log("Invalid!");
            if ($phone.hasClass("valid")) {
                console.log("removing valid");
                $phone.removeClass("valid");
            }
            $phone.addClass("invalid");
        } else {
            console.log("Valid!");
            $phone.addClass("valid");
        }
    });

    $("#emailid").on("blur", function (event) {
        var $email;
        console.log(event);

        $email = $(event.target);
        console.log($email);

        value = $email.val();
        console.log(value);

        if (value === "" || !emailPattern.test(value)) {
            //if value is empty string or fails test for phone pattern
            console.log("Invalid!");
            if ($email.hasClass("valid")) {
                console.log("removing valid");
                $email.removeClass("valid");
            }
            $email.addClass("invalid");
        } else {
            console.log("Valid!");
            if ($email.hasClass("invalid")) {
                console.log("removing invalid");
                $email.removeClass("invalid");
            }

            $email.addClass("valid");
        }
    });
    // Write your email address validation here
    // It should change the class to invalid
    // if it does not match phonePattern
});
