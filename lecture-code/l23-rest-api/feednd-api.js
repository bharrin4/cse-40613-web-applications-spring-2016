/**
 * Created by jesus on 4/7/16.
 */
"use strict";
var express = require('express'),
    Firebase = require('firebase');

var app = express(),
    dataRef = new Firebase("https://blistering-fire-6598.firebaseio.com/");

var bodyParser = require('body-parser');


// set up handlebars view engine
var handlebars = require('express-handlebars') // handlebars was installed by npm
    .create({ defaultLayout:'main' }); // Handlebars audomatically finds main.handlebars
                                       // because of the app directory structure

app.engine('handlebars', handlebars.engine); // Tell the app to use handlebars to
                                             // compile .handlebars files
app.set('view engine', 'handlebars'); // Tell the app to use handlebars by default
                                      // to compile views.


app.set('port', process.env.PORT || 3000);


// Tell the app that our static assets (images, sylesheets, javascript, etc.)
// can be found in the public folder at our project root (e.g. ./public)
app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// normal web routes go here
// ...

// API routes go here

app.get('/api/orders', function(req, res) {
    console.log("You are asking for all orders in database");
    res.json({"randomorder" : true});
});

app.get('/api/orders/:uid', function(req,res) {
    console.log("You are asking for orders for user with uid",req.params.uid);
    res.json({"ordersfor" : req.params.uid});
});

app.post('/api/orders/:uid',function(req,res) {
    console.log("You want to create a new order for user with uid",req.params.uid);
    console.log("Request body:",req.body);
    console.log("Request body rid",req.body.rid);
    res.json({"neworder" : Math.trunc(Math.random()*1000)});
});
// 404 catch-all handler (middleware)
// req and res are the same objects as in our get() handlers. next is a pointer
// to the next middleware function in the app, in this case the 500 error
// handler. Calling next() here will immediately invoke the 500 error handler.
app.use(function(req, res, next){
    res.status(404);	// Set our response header to status 404
    res.json({'error': {code:404}});
});

// 500 error handler (middleware)
app.use(function(err, req, res, next){
    console.error(err.stack);	// err is a server error, so log to see what went wrong
    res.status(500);			// Set our response header to status 500
    res.json({'error': {code:500}});
});

// Begin listening for user requests on the port we set previously. The server
// will run until it crashes or it is interrupted with Ctrl-C.
app.listen(app.get('port'), function(){
    console.log( 'Express started on http://localhost:' +
        app.get('port') + '; press Ctrl-C to terminate.' );
});


