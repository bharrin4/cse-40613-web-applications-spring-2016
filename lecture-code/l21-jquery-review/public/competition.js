/**
 * Created by jesus on 3/28/16.
 */
"use strict";
function FavoriteCars(fbname) {
    var ref = new Firebase("https://" + fbname + ".firebaseio.com/");
    this.ref = ref;
    var carsRef = ref.child('cars');
    this.carsRef = carsRef;
    var namesRef = ref.child('names');
    this.namesRef = namesRef;
    var instance = this;

    this.vote = function (carId, voteVal) {
        carsRef.child(carId).transaction(function (currentValue) {
            return (currentValue || 0) + +voteVal;
        });
    };
}

$(function () {
    var cars = new FavoriteCars("myvideo-app"),
        $vote,
        names;
    // this updates the number of votes in static list
    //cars.carsRef.orderByValue().on('value', function (snapshot) {
    //    var $jQsel;
    //    snapshot.forEach(function (childSnapshot) {
    //        var carName = childSnapshot.key(),
    //            carVotes = childSnapshot.val();
    //        console.log(carName, carVotes);
    //        $jQsel = $('#' + carName);
    //        $jQsel.find("span.badge").html(carVotes);
    //    });
    //});
    ////// click handler for both up and down votes
    //$vote = $('.vote');
    //$vote.on('click', function (e) {
    //    console.log(e.target);
    //    var $target = $(e.target);
    //    var $li = $target.parent();
    //    var carId = $li.attr('id');
    //    var voteVal = $target.attr('data-val');
    //    cars.vote(carId, +voteVal);
    //});
    // alternatively, we can keep it in order dynamically
    cars.namesRef.once("value", function (snapshot) {
        names = snapshot.val();
        console.log("read names", names);
        cars.carsRef.orderByValue().on('value', function (snapshot) {
            var $jQsel,
                $list = $('.links-list'),
                $allItems = $list.children();
            $allItems.detach();
            snapshot.forEach(function (childSnapshot) {
                var carName = childSnapshot.key(),
                    carVotes = childSnapshot.val();
                console.log(carName, carVotes);
                // change prepend to append to show it in ascending order
                $list.prepend(
                    $('<li id=' + '"' + carName + '" class="list-group-item link">' +
                        '<span class="glyphicon glyphicon-triangle-top up vote" data-val="1"></span>' +
                        '<span class="glyphicon glyphicon-triangle-bottom down vote" data-val="-1"></span>' +
                        names[carName] +
                        '<span class="badge">' + carVotes + '</span>'));
            });
            $vote = $('.vote');
            $vote.on('click', function (e) {
                console.log(e.target);
                var $target = $(e.target);
                var $li = $target.parent();
                var carId = $li.attr('id');
                var voteVal = $target.attr('data-val');
                cars.vote(carId, +voteVal);
            });
        });
    });
});